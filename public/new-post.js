// Example starter JavaScript for disabling form submissions if there are invalid fields
(function () {
  // eslint-disable-next-line semi
  'use strict'

  window.addEventListener('load', function () {
    // Set up FeathersJS app
    var app = feathers();

    // Set up REST client
    var restClient = feathers.rest();

    // Configure an AJAX library with that client
    app.configure(restClient.fetch(window.fetch));

    // Connect to the `posts` service
    const posts = app.service('posts');

    // Fetch all the forms we want to apply custom Bootstrap validation styles to
    var forms = document.getElementsByClassName('needs-validation');

    // Loop over them and prevent submission
    Array.prototype.filter.call(forms, function (form) {
      form.addEventListener('submit', function (event) {
        event.preventDefault();
        event.stopPropagation();
        if (form.checkValidity()) {
          posts.create({
            author: $('#author').val(),
            title: $('#title').val(),
            body: $('#body').val()
          });
          form.classList.remove('was-validated');
          form.reset();
        } else {
          form.classList.add('was-validated');
        }
      }, false);
    });
  }, false);
}());
