// Example starter JavaScript for disabling form submissions if there are invalid fields
(function () {
  // eslint-disable-next-line semi
  'use strict'

  window.addEventListener('load', async function () {
    // Set up FeathersJS app
    var app = feathers();

    // Set up REST client
    var restClient = feathers.rest();

    // Configure an AJAX library with that client
    app.configure(restClient.fetch(window.fetch));

    // Connect to the `posts` service
    const posts = app.service('posts');

    // Connect to the `views` service
    const views = app.service('views');

    views.create({
      windowWidth: window.innerWidth,
      windowHeight: window.innerHeight,
      userAgent: window.navigator.userAgent
    });

    // Adds a post
    const addPost = post => {
      $('#posts').append(
        `<article class="blog-post"><h2 class="blog-post-title">${post.title}</h2>
          <p class="blog-post-meta">2021-01-30T03:27:49.529Z by ${post.author}</p>
          <p>${post.body}</p></article>`
      );
    };

    // Shows the posts
    const showPosts = async postService => {
      // Find the latest 10 posts. They will come with the newest first
      const posts = await postService.find({
        query: {
          $sort: { createdAt: -1 },
          $limit: 10
        }
      });

      // We want to show the newest post last
      posts.data.reverse().forEach(addPost);
    };

    // Show existing posts
    showPosts(posts);

  }, false);
}());
