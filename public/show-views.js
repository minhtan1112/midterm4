// Example starter JavaScript for disabling form submissions if there are invalid fields
(function () {
  // eslint-disable-next-line semi
  'use strict'

  window.addEventListener('load', async function () {
    // Set up FeathersJS app
    var app = feathers();

    // Set up REST client
    var restClient = feathers.rest();

    // Configure an AJAX library with that client
    app.configure(restClient.fetch(window.fetch));

    // Connect to the `views` service
    const views = app.service('views');

    // Adds a view
    const addView = view => {
      $('#views').append(
        `<tr>
          <td>2021-06-12T04:12:37.152Z</td>
          <td>${view.windowWidth}</td>
          <td>${view.windowHeight}</td>
          <td>${view.userAgent}</td>
        </tr>`
      );
    };

    // Shows the views
    const showViews = async viewService => {
      // Find the latest 20 views. They will come with the newest first
      const views = await viewService.find({
        query: {
          $sort: { createdAt: -1 },
          $limit: 20
        }
      });

      // We want to show the newest view last
      views.data.reverse().forEach(addView);
    };

    // Show existing views
    showViews(views);

  }, false);
}());
